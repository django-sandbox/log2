package com.django.log2.logging;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Log {
    private class ConsoleColours {
        static final String ANSI_RESET = "\u001B[0m";

        // Text colours
        static final String ANSI_BLACK = "\u001B[30m";
        static final String ANSI_RED = "\u001B[31m";
        static final String ANSI_GREEN = "\u001B[32m";
        static final String ANSI_YELLOW = "\u001B[33m";
        static final String ANSI_BLUE = "\u001B[34m";
        static final String ANSI_PURPLE = "\u001B[35m";
        static final String ANSI_CYAN = "\u001B[36m";
        static final String ANSI_WHITE = "\u001B[37m";

        // Background colours
        static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
        static final String ANSI_RED_BACKGROUND = "\u001B[41m";
        static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
        static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
        static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
        static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
        static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
        static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
    }

    private static int priorityLevel = 0;
    private static final String[] PRIORITIES = {
            "VERBOSE",
            "DEBUG",
            "INFO",
            "WARNING",
            "ERROR",
            "FATAL",
            "SILENT"
    };

    private static final String DEBUG_BASE = "%s |-%s in %s - %s";
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS", Locale.getDefault());

    public static void v(Class src, String msg) {
        log(src.getName(), msg, 0);
    }
    public static void d(Class src, String msg) {
        log(src.getName(), msg, 1);
    }
    public static void i(Class src, String msg) {
        log(src.getName(), msg, 2);
    }
    public static void w(Class src, String msg) {
        log(src.getName(), msg, 3, ConsoleColours.ANSI_YELLOW);
    }
    public static void e(Class src, String msg) {
        log(src.getName(), msg, 4, ConsoleColours.ANSI_RED);
    }
    public static void f(Class src, String msg) {
        log(src.getName(), msg, 5, ConsoleColours.ANSI_RED_BACKGROUND);
    }
    public static void s(Class src, String msg) {
        log(src.getName(), msg, 6, ConsoleColours.ANSI_WHITE_BACKGROUND);
    }

    private static void log(String name, String msg, int priority) {
        if(priorityLevel > priority)
            return;
        System.out.println(String.format(DEBUG_BASE, timeFormat.format(System.currentTimeMillis()), PRIORITIES[priority], name, msg));
    }
    private static void log(String name, String msg, int priority, String ANSI_COLOUR) {
        if(priorityLevel > priority)
            return;
        System.out.println(ANSI_COLOUR + String.format(DEBUG_BASE, timeFormat.format(System.currentTimeMillis()), PRIORITIES[priority], name, msg) + ConsoleColours.ANSI_RESET);
    }

    /**
     * Set the priority of logs to be shown
     * @param level (messages lower than level will not be shown)
     *              0 shows all
     *              6 shows extremely important only
     */
    public static void setPriorityLevel(int level) {
        priorityLevel = level;
        if(priorityLevel > 6)
            priorityLevel = 6;
        if(priorityLevel < 0)
            priorityLevel = 0;
        Log.i(Log.class, "Log priority cutoff set to " + priorityLevel);
    }
    public static int getPriorityLevel() {
        return priorityLevel;
    }

    public static void setPriority(String levelName) {
        for (int i = 0; i < PRIORITIES.length; i++) {
            if(!PRIORITIES[i].equals(levelName)) continue;
            setPriorityLevel(i);
            break;
        }
    }
    public static String getPriority() {
        return PRIORITIES[priorityLevel];
    }
    public static String[] getPriorityNames() {
        return PRIORITIES;
    }
}
