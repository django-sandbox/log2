package com.django.log2.logging;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class LogTest {
    @Test
    void manualColours() {
        Log.i(getClass(), "Info log test");
        Log.w(getClass(), "Warning test");
        Log.e(getClass(), "Error test");
        Log.f(getClass(), "Fatal error test");
        Log.s(getClass(), "Silent log test");
    }
    @ParameterizedTest
    @ValueSource(strings = { "INFO", "VERBOSE", "ERROR", "SILENT", "DEBUG", "FATAL"} )
    void testNamedSetter(String name) {
        Log.setPriority(name);
        assert Log.getPriority().equals(name);
    }
}
