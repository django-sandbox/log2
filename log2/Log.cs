﻿using System;
using System.Globalization;

namespace log2
{
    public class Log
    {
        private static int _priorityLevel = 0;
        private static readonly string[] Priorities =
        {
            "VERBOSE",
            "DEBUG",
            "INFO",
            "WARNING",
            "ERROR",
            "FATAL",
            "SILENT"
        };

        private const string DebugBase = "{0} |-{1} in {2} - {3}";

        public static void V(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 0);
        }

        public static void D(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 1);
        }
        public static void I(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 2);
        }
        public static void W(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 3, ConsoleColor.Yellow);
        }
        public static void E(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 4, ConsoleColor.Red);
        }
        public static void F(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 5, backgroundColour: ConsoleColor.Red);
        }
        public static void S(object src, string msg)
        {
            Post(src.GetType().FullName, msg, 6, ConsoleColor.Black, ConsoleColor.White);
        }
        
        private static void Post(string name, string msg, int priority, ConsoleColor foregroundColour = ConsoleColor.White, ConsoleColor? backgroundColour = null)
        {
            if (_priorityLevel > priority)
                return;
            if(backgroundColour == null)
                Console.ResetColor();
            else
                Console.BackgroundColor = (ConsoleColor) backgroundColour;
            Console.ForegroundColor = foregroundColour;
            Console.WriteLine(string.Format(DebugBase, DateTime.Now.ToString(CultureInfo.CurrentCulture), Priorities[priority], name, msg));
        }

        /**
         * Set the priority of logs to be shown
         * @param level (messages lower than level will not be shown
         *                 0 shows all
         *                 6 shows extremely important only
         */
        public static void SetPriorityLevel(int level)
        {
            _priorityLevel = level;
            if (_priorityLevel > 6)
                _priorityLevel = 6;
            else if (_priorityLevel < 0)
                _priorityLevel = 0;
            Log.I(typeof(Log), string.Format("Log priority cutoff set to {0}", _priorityLevel));
        }

        public static int GetPriorityLevel()
        {
            return _priorityLevel;
        }

        public static void SetPriority(string levelName)
        {
            for (var i = 0; i < Priorities.Length; i++)
            {
                if (Priorities[i] != levelName) continue;
                SetPriorityLevel(i);
                break;
            }
        }
        public static string GetPriority()
        {
            return Priorities[_priorityLevel];
        }

        public static string[] GetPriorityNames()
        {
            return Priorities;
        }
    }
}